<?php

require __DIR__ . "/vendor/autoload.php";



return call_user_func(function() {

	$configurator = new Nette\Configurator();

	$configurator->enableDebugger(__DIR__ . "/log");
	$configurator->setTimeZone("Europe/Prague");
	$configurator->setTempDirectory(__DIR__ . "/temp");
	$configurator->addConfig(__DIR__ . "/src/config.neon");
	$configurator->addConfig(__DIR__ . "/config.local.neon");

	$container = $configurator->createContainer();

	return $container;
});
